﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public float spawnTime;
	public float xOffset;
	public GameObject obstacle;
	public GameObject player;
	public Text gameOverText;
	public Text scoreText;
	public Text restartText;
	public float yVariation;

	public float scoreIncreaseSpeed;

	private float score;
	private AudioSource[] audioSources;
	private AudioSource backgroundLoop;
	private AudioSource gameOverSound;
	private float timeSinceSpawn;


	// Use this for initialization
	void Start () {
		timeSinceSpawn = 0;
		SpawnObstacle ();
		gameOverText.text = "";
		restartText.text = "";
		score = 0;
		audioSources = GetComponents<AudioSource> ();
		backgroundLoop = audioSources [0];
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceSpawn += Time.deltaTime;
		score += Time.deltaTime * scoreIncreaseSpeed;
		if (timeSinceSpawn >= spawnTime) {
			SpawnObstacle ();
			timeSinceSpawn = 0;
		}
		if (player.GetComponent<PlayerController> ().IsDead()) {
			gameOverText.text = "Game Over";
			restartText.text = "Press 'R' to Restart";
			Time.timeScale = 0;
			backgroundLoop.Stop ();
		}
		UpdateScoreText ();
		if (Input.GetKeyDown (KeyCode.R)) {
			Time.timeScale = 1;
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	void UpdateScoreText() {
		scoreText.text = "Score: " + (int)score;
	}

	void SpawnObstacle() {
		Instantiate (obstacle, new Vector3 (xOffset, Random.Range(-yVariation,yVariation), 0), Quaternion.identity);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour {

	void OnTriggerExit2D(Collider2D other) {
		//If the object goes outside of the boundary then destroy it
		if (other.gameObject.CompareTag("Obstacle")) {
			Destroy(other.gameObject);
		}
		//If it is the player that goes outside the boundary then
		//end the game
		if (other.CompareTag ("Player")) {
			other.gameObject.GetComponent<PlayerController> ().KillPlayer ();
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float jumpForce;

	private Rigidbody2D rb;
	private bool isDead;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		isDead = false;
		audio = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {

		DebugStuff ();

		if (Input.GetKeyDown (KeyCode.Space)) {
			rb.velocity = (Vector2.zero);
			rb.AddForce (Vector2.up * jumpForce);
		}
	}

	public void KillPlayer() {
		isDead = true;
		audio.Play ();
	}

	public bool IsDead() {
		return isDead;
	}

	void DebugStuff() {
		Debug.DrawRay (this.transform.position, new Vector3 (10, 0), Color.red);
		Debug.DrawRay (this.transform.position, new Vector3 (0, 10), Color.red);
		Debug.DrawRay (this.transform.position, new Vector3 (-10, 0), Color.red);
		Debug.DrawRay (this.transform.position, new Vector3 (0, -10), Color.red);
	}
}

﻿using UnityEngine;
using System.Collections;

public class KillOnCollision : MonoBehaviour {

	private PlayerController pc;
	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		pc = GetComponent<PlayerController> ();
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.CompareTag ("Obstacle")) {
			pc.KillPlayer ();
			rb.isKinematic = true;
		}
	}
}
